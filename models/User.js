const Sequelize = require('sequelize');
const connection = require('../config/DBConnection');
var exports = module.exports = {};

const User = connection.sequelize.define('User',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_name: {
            type: Sequelize.STRING(50)
        },
        name: {
            type: Sequelize.STRING(50)
        },
        birth_date: {
            type: Sequelize.DATE
        },
        gender: {
            type: Sequelize.SMALLINT
        },
        phone: {
            type: Sequelize.STRING(50)
        },
        birth_loc: {
            type: Sequelize.DATE
        },
        email: {
            type: Sequelize.STRING(100)
        },
        photo: {
            type: Sequelize.STRING(255)
        },
        created_at: {
            type: Sequelize.DATE
        },
        push_allowed: {
            type: Sequelize.BOOLEAN
        },
        firebase_id: {
            type: Sequelize.STRING(100)
        },
    }, {
        freezeTableName: true
    });

exports.User = User;