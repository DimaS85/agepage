const restify = require('restify'),
      restifyPlugins = require('restify-plugins'),
      versioning = require('restify-url-semver'),
      bunyan = require('bunyan'),
      RotatingFileStream = require('bunyan-rotating-file-stream');

require('dotenv').config();

const accessMiddleware = require('./helpers/accessUserMiddleware'),
      config = require('./config/config');

const log = bunyan.createLogger({
    name: 'AgePage Client API',
    streams: [
        {
            level: 'debug',
            stream: process.stdout       // log INFO and above to stdout
        },
        {
            level: 'info',
            path: config.logPath + 'info.log'
        },
        {
            level: 'error',
            path: config.logPath + 'errors.log'
        },
        {
            type: 'raw',
            stream: new RotatingFileStream({
                path: config.logPath + `errors.log`,
                period: '1w',          // daily rotation
                totalFiles: 3,        // keep 10 back copies
                rotateExisting: true,  // Give ourselves a clean file when we start up, based on period
                threshold: '10m',      // Rotate log files larger than 10 megabytes
                totalSize: '20m',      // Don't keep more than 20mb of archived log files
                gzip: true             // Compress the archive log files to save space
            })
        }
    ],
    serializers: bunyan.stdSerializers
});

function reqSerializer(req, err) {
    return {
        method: req.method,
        url: req.url,
        err: err.message
    };
}

const options = {
    name: "AgePage Client API",
    log: log,
    version: ['1.0.0', '2.0.0']
};

const server = restify.createServer(options);

server.use(restifyPlugins.acceptParser(server.acceptable));
server.use(restifyPlugins.queryParser());
server.use(restifyPlugins.bodyParser());
server.pre((req,res,next) => accessMiddleware.checkAccessForUser(req,res,next));
server.pre(versioning());

function errorLogger(req, res, err) {
    log.error(reqSerializer(req, err));
    // log.error({req, res, err});
    res.send(500, { "message" : "Internal Server Error" });
}
server.on('NotFound', (req, res, err) => errorLogger(req, res, err));
server.on('InternalServer', (req, res, err) => errorLogger(req, res, err));
server.on('restifyError', (req, res, err, next) => errorLogger(req, res, err));
server.on('uncaughtException', (req, res, err) => errorLogger(req, res, err));


// init routes
const routesv1 = require("./routesv1/index");

initAllRoutes(routesv1.allRoutes);
function initAllRoutes(handlers) {
    handlers.map((handler) => {
        require(handler).call(null, server);
    });
}

server.listen(config.port, () => {
    console.log('%s listening at %s, on %s, on %s', server.name, server.url, process.env.DB_HOST, process.env.DB_NAME);
});