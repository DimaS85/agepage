const sequelize = require('sequelize');
const Op = sequelize.Op;
const moment = require('moment');

const ModelProvider = require('../../helpers/modelProvider');
const setLimitAndOffset = require('../../helpers/setLimitAndOffset');
const userActions = require('../../helpers/userActions');
const queryParams = require('../../helpers/queryParams');
const cinemigo = require('../../helpers/cinemigo');

function MyMovieRoutes(server) {
    server.get({path: "/me/movies", version: '2.0.0'}, (req, res, next) => {
        let movieType = (req.query.movie_type) ? req.query.movie_type : 'cinema',
            yearFilter = (req.query.year) ? req.query.year : false,
            genreFilter = (req.query.genres) ? req.query.genres.split(',') : false,
            cinemaIMDBRating = (genreFilter && genreFilter.length == 1) ? 6.7 : 7.2,
            digitalInnerRating = (genreFilter && genreFilter.length == 1) ? 71 : 76,
            digitalIMDBRating = (genreFilter && genreFilter.length == 1) ? 6.5 : 7;

        function formWhereConditions() {
            let whereConditions = {
                // exclude voted or movies with status not going to watch (vote = 3)
                [Op.or]: [{'$VotedMovie.vote$': 4}, {'$VotedMovie.vote$': null}]
            };

            // check movie type and set conditions
            if(movieType == 'cinema'){
                whereConditions["movie_details.isRecommendedInCinema"] = true;

                // exclude movies with imdb_rating
                whereConditions.imdb_rating = { [Op.gt]: cinemaIMDBRating };

            } else if (movieType == 'digital'){
                whereConditions["movie_details.isRecommendedInDigital"] = true;

                // exclude movies with length < 50, but not exclude movies without length
                // whereConditions.length = {
                //     [Op.or]: [
                //         { [Op.gt]: 50 },
                //         null
                //     ]
                // };

                // exclude movies for inner_rating
                whereConditions.inner_rating = { [Op.gte]: digitalInnerRating };

                // exclude movies for imdb_rating
                whereConditions.imdb_rating = { [Op.gt]: digitalIMDBRating };

                // exclude movies in cinema from digital
                whereConditions["movie_details.isRecommendedInCinema"] = {[Op.or]: [false, null]};
            }

            // add filter by year if his set
            if(yearFilter)
                whereConditions.release_date = {[Op.between]: [`${yearFilter}-01-01`, `${yearFilter}-12-31`]};

            // add filter by genres if his set
            if(genreFilter)
                whereConditions['$MovieGenres.genre_id$'] = genreFilter;

            return whereConditions;
        }

        function formIncludeConditions() {
            let include = [
                {
                    model: ModelProvider.MovieVote,
                    as: 'VotedMovie',
                    attributes: [],
                    where: { user_id: req.user.id },
                    required: false
                }
            ];

            // include MovieGenres table if set genreFilter
            if(genreFilter){
                include.push({
                    model: ModelProvider.MovieGenre,
                    as: 'MovieGenres',
                    attributes: [],
                    required: true
                })
            }

            return include;
        }

        function getMovies(where, include, moviesIds = null) {
            if(moviesIds != null){
                where.inner_rating = { [Op.gte]: digitalInnerRating };
                where.id = { [Op.notIn]: moviesIds };
                return ModelProvider.Movie.findAndCountAll({
                    attributes: ['id', 'title', 'release_date', [sequelize.col('VotedMovie.vote'), 'vote']],
                    where,
                    distinct: "id",
                    include,
                    group: ['Movie.id', 'VotedMovie.vote'],
                    order: [['release_date', 'DESC', 'NULLS LAST']],
                    // offset: setLimitAndOffset.setOffset(req.query.offset),
                    offset: 0,
                    // limit: setLimitAndOffset.setLimit(req.query.limit),
                    limit: 30 - moviesIds.length,
                    subQuery: false
                }).then(movies => { return movies })
            } else {
                return ModelProvider.Movie.findAndCountAll({
                    attributes: ['id', 'title', 'release_date', [sequelize.col('VotedMovie.vote'), 'vote']],
                    where,
                    distinct: "id",
                    include,
                    group: ['Movie.id', 'VotedMovie.vote'],
                    order: [['release_date', 'DESC', 'NULLS LAST']],
                    // offset: setLimitAndOffset.setOffset(req.query.offset),
                    offset: 0,
                    // limit: setLimitAndOffset.setLimit(req.query.limit),
                    limit: 30,
                    subQuery: false
                }).then(movies => { return movies })
            }
        }
        
        function setAdditionalParams(movies) {
            let count = Object.keys(movies.count).length,
                results = movies.rows,
                userId = req.user.id;

            return setVotesCountsForMovies(results, userId)
                .then(results => setCinemogosVotesCountsForMovie(results, userId))
                .then(results => {
                    res.send({count, results});
                    next();
                });
        }

        function setAdditionalParamsForAllMovies(movies) {
            let count = movies.length,
                userId = req.user.id;

            setVotesCountsForMovies(movies, userId)
                .then(results => setCinemogosVotesCountsForMovie(results, userId))
                .then(results => {
                    // sort by release date
                    results.sort((movie1, movie2) => {
                        return movie2.dataValues.release_date - movie1.dataValues.release_date;
                    });

                    res.send({count, results});
                    next();
                });
        }

        function getMoviesByGenres(where, include) {
            // set digitalInnerRating by genres count
            where.inner_rating = { [Op.gte]: digitalInnerRating - genreFilter.length};

            function getMoviesIds() {
                return ModelProvider.Movie.findAll({
                    attributes: ['id', 'title', 'release_date',
                        [sequelize.col('VotedMovie.vote'), 'vote']
                    ],
                    where,
                    include,
                    subQuery: false
                }).then(movies => {
                    let moviesIdsArr = [];
                    movies.forEach(movie => moviesIdsArr.push(movie.id));

                    return moviesIdsArr;
                })
            }

            function getMovieGenre (movie_id){
                return ModelProvider.MovieGenre.findAndCountAll({
                    attributes: ['movie_id'],
                    where: {movie_id, genre_id: genreFilter},
                    group: ['MovieGenre.movie_id']
                }).then(movies => {
                    //set counts for movies
                    let counts = movies.count,
                        results = [];

                    movies.rows.forEach((movie, index) => {
                        movie.dataValues.count = parseInt(counts[index].count);
                        if (parseInt(counts[index].count) == genreFilter.length)
                            results.push(movie.dataValues);
                    });

                    return results;
                })
            }

            return getMoviesIds()
                .then(getMovieGenre);
        }

        function getMoviesWithSeveralGenres(moviesIds, include) {
            return ModelProvider.Movie.findAndCountAll({
                attributes: ['id', 'title', 'release_date', [sequelize.col('VotedMovie.vote'), 'vote']],
                where: { id: moviesIds },
                distinct: "id",
                include,
                group: ['Movie.id', 'VotedMovie.vote'],
                order: [['release_date', 'DESC', 'NULLS LAST']],
                subQuery: false
            }).then(movies => { return movies })
        }

        let whereConditions = formWhereConditions(),
            includeConditions = formIncludeConditions();

        if(movieType == 'digital' && genreFilter && genreFilter.length > 1 && genreFilter.length <= 5){
            getMoviesByGenres(whereConditions, includeConditions)
                .then(movies => {
                    if(movies.length != 0){
                        // get movies ids
                        let moviesIds = [];
                        movies.forEach(movie => {
                            moviesIds.push(movie.movie_id);
                        });

                        return Promise.all([
                            getMoviesWithSeveralGenres(moviesIds, includeConditions),
                            getMovies(whereConditions, includeConditions, moviesIds)
                        ]).then(data => {
                            let moviesWithSeveralGenres = data[0].rows,
                                othersMovies = data[1].rows,
                                allMoviesArr = moviesWithSeveralGenres.concat(othersMovies);

                            setAdditionalParamsForAllMovies(allMoviesArr);
                           });
                    } else {
                        return getMovies(whereConditions, includeConditions)
                            .then(setAdditionalParams)
                            .catch(err => next(err));
                    }
                }).catch(err => next(err));
        } else {
            getMovies(whereConditions, includeConditions)
                .then(setAdditionalParams)
                .catch(err => next(err));
        }
    });
}

function setVotesCountsForMovies(movies, curUser) {
    function getUsersMoviesVotesCounts(moviesIdsArr, cinemigosIdsArr, vote = [1,2,3,4]) {
        return ModelProvider.MovieVote.findAll({
            attributes: ['movie_id', [sequelize.fn('COUNT', sequelize.col('movie_id')), 'votes_count']],
            where: { movie_id: moviesIdsArr, user_id: cinemigosIdsArr, vote },
            group: ['movie_id']
        }).then(movies => { return movies });
    }

    function findVotesCounts(users) {
        // form movies id array
        let moviesIdsArr = [];
        movies.forEach(movie => moviesIdsArr.push(movie.id));

        // form users id array
        let usersIdsArr = [];
        users.forEach(user => usersIdsArr.push(user.id));

        return Promise.all([
            getUsersMoviesVotesCounts(moviesIdsArr, usersIdsArr, 1),
            getUsersMoviesVotesCounts(moviesIdsArr, usersIdsArr, [1, 2])
        ]).then(votesMovies => {
            // console.log('votesMovies: ', votesMovies);

            let likedVoteMoviesArr = votesMovies[0],
                allVoteMoviesArr = votesMovies[1];

            movies.forEach(movie => {
                let movieId = movie.dataValues.id;

                if(likedVoteMoviesArr.length > 0){
                    //set likes vote count to all movies
                    for(let movie2 of likedVoteMoviesArr){
                        let movieIdLikedVote = movie2.dataValues.movie_id;

                        if(movieId == movieIdLikedVote){
                            movie.dataValues.liked_votes_count = parseInt(movie2.dataValues.votes_count);
                            break;
                        } else movie.dataValues.liked_votes_count = 0;
                    }
                } else movie.dataValues.liked_votes_count = 0;

                if(allVoteMoviesArr.length > 0){
                    //set all vote count to all movies
                    for(let movie2 of allVoteMoviesArr){
                        let movieIdLikedVote = movie2.dataValues.movie_id;

                        if(movieId == movieIdLikedVote){
                            movie.dataValues.all_votes_count = parseInt(movie2.dataValues.votes_count);
                            break;
                        } else movie.dataValues.all_votes_count = 0;
                    }
                } else movie.dataValues.all_votes_count = 0;
            });
            return movies;
        });
    }

    return get100UsersByRelationOrPopularity(curUser)
        .then(findVotesCounts);
}

function setCinemogosVotesCountsForMovie(movies, curUser) {
    //form moviesIdsArr
    let moviesIdsArr = [];
    movies.forEach(movie => moviesIdsArr.push(movie.id));

    function getCinemigosMoviesVotesCounts(moviesIdsArr, cinemigosIdsArr, vote) {
        return ModelProvider.MovieVote.findAll({
            attributes: ['movie_id', [sequelize.fn('COUNT', sequelize.col('movie_id')), 'votes_count']],
            where: { movie_id: moviesIdsArr, user_id: cinemigosIdsArr, vote },
            group: ['movie_id']
        }).then(movies => { return movies });
    }

    function getAllCounts(cinemigos) {
        let totalCinemigosCount = cinemigos.count,
            cinemigosIdsArr = [];

        //form cinemigosIdsArr
        cinemigos.results.forEach(cinemigo => cinemigosIdsArr.push(cinemigo.id));

        return Promise.all([
            getCinemigosMoviesVotesCounts(moviesIdsArr, cinemigosIdsArr, 4),
            getCinemigosMoviesVotesCounts(moviesIdsArr, cinemigosIdsArr, [1,2,3])
        ]).then(movies => {
            let goingToWatchMoviesArr = movies[0],
                otherVotesMoviesArr = movies[1];

            return {
                totalCinemigosCount,
                goingToWatchMoviesArr,
                otherVotesMoviesArr
            }
        })
    }

    function setValuesToMovies(data) {
        movies.forEach(movie => {
            let movieId = movie.id;

            if(data.goingToWatchMoviesArr.length > 0){
                //set going to watch vote count
                for(let movie2 of data.goingToWatchMoviesArr){
                    let movieIdWithGoingToWatchVote = movie2.dataValues.movie_id;
                    if(movieId == movieIdWithGoingToWatchVote){
                        movie.dataValues.cinemigos_gtw_count = parseInt(movie2.dataValues.votes_count);
                        break;
                    } else movie.dataValues.cinemigos_gtw_count = 0;
                }
            } else movie.dataValues.cinemigos_gtw_count = 0;

            if(data.otherVotesMoviesArr > 0){
                //set other votes count
                for(let movie2 of data.otherVotesMoviesArr){
                    let movieIdWithGoingToWatchVote = movie2.dataValues.movie_id;
                    if(movieId == movieIdWithGoingToWatchVote){
                        movie.dataValues.cinemigos_vote_count = parseInt(movie2.dataValues.votes_count);
                        break;
                    } else movie.dataValues.cinemigos_vote_count = 0;
                }
            } else movie.dataValues.cinemigos_vote_count = 0;

            //set total cinemigos count
            movie.dataValues.cinemigos_total_count = data.totalCinemigosCount;
        });

        return movies;
    }
}


function get100UsersByRelationOrPopularity(curUser) {
    function getFaves(user_id) {
        return ModelProvider.UserFriend.findAll({
            attributes:['friend_id'],
            where: { user_id },
            include: {
                model: ModelProvider.User,
                as: 'Faves',
                attributes: []
            },
            order: [[{ model:ModelProvider.User, as: 'Faves' }, 'followers_count', 'DESC', 'NULLS LAST']]
        }).then(users => {
            let usersIdsArr = [];
            users.forEach(user =>{
                usersIdsArr.push(user.friend_id)
            });
            return usersIdsArr;
        });
    }

    function getFBFriends() {
        return ModelProvider.UserRecommendedUser.findAll({
            attributes: ['friend_id'],
            where: {
                user_id: curUser,
                type: 1
            },
            include: {
                model: ModelProvider.User,
                as: 'UserRecommendedUserInfo',
                attributes: []
            },
            order: [[{ model:ModelProvider.User, as: 'UserRecommendedUserInfo' }, 'followers_count', 'DESC', 'NULLS LAST']]
        }).then(users => {
            let usersIdsArr = [];
            users.forEach(user => {
                usersIdsArr.push(user.friend_id);
            });
            return usersIdsArr;
        })
    }

    function getTopUsers(where, userType) {
        return ModelProvider.User.findAll({
            attributes: ['id'],
            where,
            order: [[userType, 'DESC', 'NULLS LAST']],
            limit: 100
        }).then(users => {
            let usersIdsArr = [];
            users.forEach(user => {
                usersIdsArr.push(user.id);
            });
            return usersIdsArr;
        })
    }

    function setUserType(usersArr, userType) {
        // 1 - cinemigos, 2 - faves, 3 - fbFriends, 4 - top 100 users by followers count
        let typeArr = [];

        usersArr.forEach(user => {
            typeArr.push({id: user, type: userType})
        });

        return typeArr;
    }

    return Promise.all([cinemigo.getCinemigos(curUser, ['id'], ['followers_count']), getFaves(curUser), getFBFriends()])
        .then(users => {
            let cinemigos = [],
                faves = users[1],
                fbFriends = users[2];

            // form cinemigos ids array
            users[0].results.forEach(cinemigo => {
                cinemigos.push(cinemigo.id)
            });

            // delete cinemigos from faves
            let onlyFaves = faves.filter(x => {
                return cinemigos.indexOf(x) < 0;
            });

            //delete faves and cinemigos from fbFriends
            let onlyFbFriendsArr = onlyFaves.concat(cinemigos),
                onlyFbFriends = fbFriends.filter(x => {
                    return onlyFbFriendsArr.indexOf(x) < 0;
                });

            let allUsers = onlyFaves.concat(cinemigos, fbFriends);
            whereObj = {id: {[Op.notIn]: allUsers}};

            return getTopUsers({id: {[Op.notIn]: allUsers}, followers_count: {[Op.gte]: 100}}, 'followers_count')
                .then(onlyTopUsers => {
                    // set type for users, 1 - cinemigos, 2 - faves, 3 - fbFriends, 4 - top 100 users by followers count
                    let cinemigosTypeArr = setUserType(cinemigos, 1),
                        onlyFavesTypeArr = setUserType(onlyFaves, 2),
                        onlyFbFriendsTypeArr = setUserType(onlyFbFriends, 3),
                        onlyTopUsersTypeArr = setUserType(onlyTopUsers, 4),
                        mainUsersArr = cinemigosTypeArr.concat(onlyFavesTypeArr, onlyFbFriendsTypeArr, onlyTopUsersTypeArr),
                        hundredMainUsersArr = mainUsersArr.slice(0, 100);

                    return hundredMainUsersArr;
                });
        }).catch(err => console.log(err));
}

module.exports = MyMovieRoutes;