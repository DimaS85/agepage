var exports = module.exports = {};

const firebaseAdmin = require("firebase-admin");

const config = require('../config/config'),
      ModelProvider = require('./modelProvider');

const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'AgePage Client API',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout       // log INFO and above to stdout
              },
              {
                  level: 'info',
                  path: config.logPath + 'info.log'
              },
              {
                  level: 'error',
                  path: config.logPath + 'errors.log'
              }
          ],
          serializers: bunyan.stdSerializers
      });

function checkAccessForUser (req,res,next) {
    if (!req.url.startsWith("/v")) {
        let url = req.url;
        req.url = `/v1${url}`;
    }

    if (req.headers['app-authorization'] == 'authorization'){
        if(req.headers.devuserid){
            req.user = { id: req.headers.devuserid};
            return next();
        }else if (req.url.search('/me') != -1 && !(req.headers.token)) {
            log.error("Sorry, no user token");
            return res.send(401, { "message" : "Sorry, no user token" });
        }else if ((req.url == "/v1/me") && (req.method === 'POST')){
            return next();
        }else if (req.url.search('/me') != -1) {
            firebaseAdmin.auth().verifyIdToken(req.headers.token)
                .then(decodedToken => {
                    ModelProvider.User.findOne({
                        attributes: ['id', 'region', 'facebook_id'],
                        where: {firebase_id: decodedToken.uid}
                    }).then(user => {
                        if (user) {
                            // set new facebook id (for new facebook app)
                            let facebookId = user.facebook_id,
                                facebook_id = (decodedToken.firebase.sign_in_provider == 'facebook.com')
                                    ? decodedToken.firebase.identities['facebook.com'][0] : null;

                            if (facebookId == null) {
                                return user.update({
                                    facebook_id
                                }).then(upUser => {
                                    req.user = {
                                        id: upUser.dataValues.id,
                                        region: upUser.dataValues.region
                                    };
                                    next();
                                    return true;
                                }).catch(err => next(err));
                            } else {
                                req.user = {
                                    id: user.dataValues.id,
                                    region: user.dataValues.region
                                };
                                next();
                                return true;
                            }
                        } else {
                            log.error("message: Sorry, no user in db");
                            return res.send(401, {"message": "Sorry, no user in db"})
                        }
                    }).catch(err => next(err));
                }).catch(() => {
                log.error("message : Sorry, invalid user token");
                res.send(401, {"message": "Sorry, invalid user token"});
            });
        } else return next();
    } else {
        log.error("You are not logged in!!!");
        return res.send(401, {"message": "You are not logged in!!!"});
    }
}

exports.checkAccessForUser = checkAccessForUser;