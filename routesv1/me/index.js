const firebaseAdmin = require("firebase-admin");

const ModelProvider = require('../../helpers/modelProvider');

function AllForMeRoute(server) {
    server.get({path: "/me", version: '1.0.0'}, function (req, res, next) {
        ModelProvider.User.findOne({
            attributes: ['id', 'user_name', 'name', 'birth_date', 'gender', 'phone', 'birth_loc', 'email', 'photo'],
            where: { id: req.user.id },
        }).then(user => {
            res.send(user);
            next();
        }).catch(err => next(err));
    });

    server.post({path: "/me", version: '1.0.0'}, function (req, res, next) {
        function createUser(userRecord) {
            return ModelProvider.User.create({
                name: userRecord.displayName,
                email: userRecord.email,
                photo: userRecord.photoURL,
                created_at: userRecord.metadata.creationTime
            }).then(user => {
                res.send({ id: user.dataValues.id });
                return user;
            }).catch(err => next(err));
        }

        function findOrCreateUser(decodedToken) {
            let userFacebookId = (decodedToken.firebase.sign_in_provider == 'facebook.com')
                ? decodedToken.firebase.identities['facebook.com'][0] : null;

            firebaseAdmin.auth().getUser(decodedToken.uid)
                .then(userRecord => {
                    ModelProvider.User.findOne({
                        where:{ firebase_id: userRecord.uid }
                    }).then(user => {
                        if (user == null) {
                            return createUser(userFacebookId, userRecord)
                                .catch(err => next(err));
                        } else
                            res.send({ id: user.dataValues.id });
                    });
                });
        }
        
        firebaseAdmin.auth().verifyIdToken(req.headers.token)
            .then(findOrCreateUser)
            .catch(err => next(err));
    });
}

module.exports = AllForMeRoute;