'use strict';

require('dotenv').config();

function getDbSettings() {
    return {
        connectionLimit: '',
        dialect: '',
        host: '',
        user: '',
        password: '',
        database: ''
    }
}
module.exports = {
    name: '',
    version: '',
    port: '',
    database: getDbSettings(),
    logPath: ''
};