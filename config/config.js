'use strict';

require('dotenv').config();

function getDbSettings() {
    return {
        connectionLimit: process.env.DB_CONNECTION_LIMIT,
        dialect: process.env.DB_DIALECT,
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME
    }
}
module.exports = {
    name: 'Age Page Api',
    version: '0.0.1',
    port: process.env.PORT || 3100,
    database: getDbSettings(),
    logPath: './logs/main/'
};